package ru.ahmetahunov.tm.api;

import ru.ahmetahunov.tm.entity.AbstractEntity;
import ru.ahmetahunov.tm.exception.IdCollisionException;

import java.util.Collection;

public interface IRepository<T extends AbstractEntity> {

    public T persist(T item) throws IdCollisionException;

    public T merge(T item);

    public void clear();

    public boolean containsValue(T item);

    public T remove(String id);

    public Collection<T> findAll();

    public T findOne(String id);

}
