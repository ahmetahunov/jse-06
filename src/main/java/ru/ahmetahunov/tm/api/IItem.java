package ru.ahmetahunov.tm.api;

import java.util.Date;

public interface IItem {

    public String getName();

    public String getDescription();

    public Date getStartDate();

    public Date getFinishDate();

}
