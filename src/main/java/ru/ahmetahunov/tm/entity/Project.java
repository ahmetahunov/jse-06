package ru.ahmetahunov.tm.entity;

import ru.ahmetahunov.tm.api.IItem;
import java.util.Date;
import java.util.Objects;

public class Project extends AbstractEntity implements IItem {

    private String name = "";

    private String description = "";

    private Date startDate = new Date(0);

    private Date finishDate = new Date(0);

    private String userId = "";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(Date finishDate) {
        this.finishDate = finishDate;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Project project = (Project) o;
        return name.equals(project.getName()) && userId.equals(project.getUserId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, userId);
    }

}
