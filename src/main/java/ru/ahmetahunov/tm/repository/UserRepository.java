package ru.ahmetahunov.tm.repository;

import ru.ahmetahunov.tm.api.IRepository;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.exception.IdCollisionException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class UserRepository implements IRepository<User> {

    private final Map<String, User> collection = new HashMap<>();

    @Override
    public User persist(User user) throws IdCollisionException {
        if (collection.containsKey(user.getId()))
            throw new IdCollisionException();
        collection.put(user.getId(), user);
        return user;
    }

    @Override
    public User merge(User user) {
        collection.put(user.getId(), user);
        return user;
    }

    @Override
    public void clear() {
        collection.clear();
    }

    @Override
    public boolean containsValue(User user) {
        return collection.containsValue(user);
    }

    @Override
    public User remove(String id) {
        return collection.remove(id);
    }

    @Override
    public Collection<User> findAll() { return collection.values(); }

    @Override
    public User findOne(String id) {
        return collection.get(id);
    }

    public User findUser(String login) {
        for (User user : findAll()) {
            if (user.getLogin().equalsIgnoreCase(login)) return user;
        }
        return null;
    }

}
