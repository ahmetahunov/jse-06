package ru.ahmetahunov.tm.repository;

import ru.ahmetahunov.tm.api.IRepository;
import ru.ahmetahunov.tm.entity.Task;
import ru.ahmetahunov.tm.exception.IdCollisionException;

import java.util.*;

public class TaskRepository implements IRepository<Task> {

    private final Map<String, Task> collection = new HashMap<>();

    @Override
    public Task persist(Task task) throws IdCollisionException {
        if (collection.containsKey(task.getId()))
            throw new IdCollisionException();
        collection.put(task.getId(), task);
        return task;
    }

    @Override
    public Task merge(Task task) {
        collection.put(task.getId(), task);
        return task;
    }

    @Override
    public void clear() {
        collection.clear();
    }

    @Override
    public boolean containsValue(Task task) {
        return collection.containsValue(task);
    }

    @Override
    public Task remove(String id){
        return collection.remove(id);
    }

    @Override
    public Collection<Task> findAll() {
        return collection.values();
    }

    @Override
    public Task findOne(String id) {
        return collection.get(id);
    }

    public void removeAll(String userId) {
        for (Task task : findAll(userId))
            remove(task.getId());
    }

    public List<Task> findAll(String userId) {
        List<Task> tasks = new LinkedList<>();
        for (Task task : findAll()) {
            if (task.getUserId().equals(userId)) tasks.add(task);
        }
        return tasks;
    }

    public List<Task> findAll(String projectId, String userId) {
        List<Task> tasks = new LinkedList<>();
        for (Task task : findAll()) {
            if (task.getUserId().equals(userId) && task.getProjectId().equals(projectId))
                tasks.add(task);
        }
        return tasks;
    }

    public Task findOne(String taskName, String projectId, String userId) {
        for (Task task : findAll()) {
            if (task.getUserId().equals(userId)
                    && task.getProjectId().equals(projectId)
                    && task.getName().equals(taskName))
                return task;
        }
        return null;
    }

}
