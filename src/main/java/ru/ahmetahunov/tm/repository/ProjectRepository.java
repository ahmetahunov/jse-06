package ru.ahmetahunov.tm.repository;

import ru.ahmetahunov.tm.api.IRepository;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.exception.IdCollisionException;

import java.util.*;

public class ProjectRepository implements IRepository<Project> {

    private final Map<String, Project> collection = new HashMap<>();

    @Override
    public Project persist(Project project) throws IdCollisionException {
        if (collection.containsKey(project.getId()))
            throw new IdCollisionException();
        collection.put(project.getId(), project);
        return project;
    }

    @Override
    public Project merge(Project project) {
        collection.put(project.getId(), project);
        return project;
    }

    @Override
    public void clear() {
        collection.clear();
    }

    @Override
    public boolean containsValue(Project project) {
        return collection.containsValue(project);
    }

    @Override
    public Project remove(String id){
        return collection.remove(id);
    }

    @Override
    public Collection<Project> findAll() {
        return collection.values();
    }

    @Override
    public Project findOne(String id) {
        return collection.get(id);
    }

    public void removeAll(String userId) {
        for (Project project : findAll(userId)) {
            remove(project.getId());
        }
    }

    public List<Project> findAll(String userId) {
        List<Project> projects = new LinkedList<>();
        for (Project project : findAll()) {
            if (project.getUserId().equals(userId)) projects.add(project);
        }
        return projects;
    }

    public Project findOne(String projectName, String userId) {
        for (Project project : findAll()) {
            if (project.getUserId().equals(userId) && project.getName().equals(projectName))
                return project;
        }
        return null;
    }

}
