package ru.ahmetahunov.tm.command.user;

import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.context.Bootstrap;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.helper.ConsoleHelper;
import ru.ahmetahunov.tm.service.UserService;
import ru.ahmetahunov.tm.util.PassUtil;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;

public class UserLogInCommand extends AbstractCommand {

    public UserLogInCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public boolean isAllowed() {
        if (bootstrap.getCurrentUser() != null) return false;
        return true;
    }

    @Override
    public String getName() {
        return "log-in";
    }

    @Override
    public String getDescription() {
        return "User log in.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[LOG IN]");
        User user = getUser();
        System.out.print("Please enter password: ");
        String password = ConsoleHelper.readMessage().trim();
        if (isAccepted(password, user.getPassword())) {
            bootstrap.setCurrentUser(user);
            System.out.println("Welcome, " + user.getLogin());
            return;
        }
        System.out.println("Wrong password.");
    }

    private User getUser() throws IOException {
        UserService userService = bootstrap.getServiceLocator().getUserService();
        User user = null;
        while (user == null) {
            System.out.print("Please enter login: ");
            String login = ConsoleHelper.readMessage().trim();
            user = userService.findUser(login.toLowerCase());
            if (user == null)
                System.out.println("User does not exist.");
        }
        return user;
    }

    private boolean isAccepted(String password, String hash) throws NoSuchAlgorithmException {
        String checkPassword = PassUtil.getHash(password);
        return hash.equals(checkPassword);
    }

}
