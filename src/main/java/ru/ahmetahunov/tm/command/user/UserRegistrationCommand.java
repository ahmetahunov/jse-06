package ru.ahmetahunov.tm.command.user;

import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.context.Bootstrap;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.helper.ConsoleHelper;
import ru.ahmetahunov.tm.service.UserService;
import ru.ahmetahunov.tm.util.PassUtil;
import ru.ahmetahunov.tm.util.RoleUtil;
import java.io.IOException;

public class UserRegistrationCommand extends AbstractCommand {

    public UserRegistrationCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public boolean isAllowed() {
        User user = bootstrap.getCurrentUser();
        if (user == null) return true;
        if (user.getRole() == Role.ADMINISTRATOR) return true;
        return false;
    }

    @Override
    public String getName() {
        return "user-register";
    }

    @Override
    public String getDescription() {
        return "New user registration.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[REGISTRATION]");
        System.out.print("Please enter login: ");
        String login = getLogin();
        System.out.print("Please enter password: ");
        String password = ConsoleHelper.readMessage().trim();
        System.out.print("Please enter new password one more time: ");
        String repeatPass = ConsoleHelper.readMessage();
        if (password.isEmpty() || !password.equals(repeatPass)) {
            System.out.println("Passwords do not match!");
            return;
        }
        password = PassUtil.getHash(password);
        User user = new User();
        user.setLogin(login);
        user.setPassword(password);
        User currentUser = bootstrap.getCurrentUser();
        if (currentUser != null && currentUser.getRole() == Role.ADMINISTRATOR)
            user.setRole(getRole());
        bootstrap.getServiceLocator().getUserService().persist(user);
        System.out.println("[OK]");
    }

    private Role getRole() throws IOException {
        System.out.print("Please enter role<User/Administrator>: ");
        String answer = ConsoleHelper.readMessage().trim();
        Role role = RoleUtil.getRole(answer);
        if (role == null) {
            System.out.println("Unknown value.");
            return getRole();
        }
        return role;
    }

    private String getLogin() throws IOException {
        UserService userService = bootstrap.getServiceLocator().getUserService();
        String login = ConsoleHelper.readMessage().trim();
        User user = userService.findUser(login.toLowerCase());
        while (user != null) {
            System.out.println("User with this login already exists!");
            System.out.print("Please enter login: ");
            login = ConsoleHelper.readMessage().trim();
            user = userService.findUser(login.toLowerCase());
        }
        return login;
    }

}
