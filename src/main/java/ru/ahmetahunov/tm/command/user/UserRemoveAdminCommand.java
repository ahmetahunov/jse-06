package ru.ahmetahunov.tm.command.user;

import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.context.Bootstrap;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.helper.ConsoleHelper;
import ru.ahmetahunov.tm.service.ProjectService;
import ru.ahmetahunov.tm.service.TaskService;
import ru.ahmetahunov.tm.service.UserService;

public class UserRemoveAdminCommand extends AbstractCommand {

    public UserRemoveAdminCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public boolean isAllowed() {
        User user = bootstrap.getCurrentUser();
        if (user == null) return false;
        if (user.getRole() == Role.ADMINISTRATOR) return true;
        return false;
    }

    @Override
    public String getName() {
        return "user-remove";
    }

    @Override
    public String getDescription() {
        return "Remove selected user and all his projects and tasks.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[USER REMOVE]");
        System.out.print("Please enter user login: ");
        String login = ConsoleHelper.readMessage().trim();
        UserService userService = bootstrap.getServiceLocator().getUserService();
        User user = userService.findUser(login.toLowerCase());
        if (user == null) {
            System.out.println("Selected user does not exist.");
            return;
        }
        ProjectService projectService = bootstrap.getServiceLocator().getProjectService();
        TaskService taskService = bootstrap.getServiceLocator().getTaskService();
        for (Project project : projectService.listAllItems()) {
            if (project.getUserId().equals(user.getId()))
                taskService.removeAllProjectTasks(project.getId(), user.getId());
        }
        projectService.removeAllUserProjects(user.getId());
        userService.remove(user);
        System.out.println("[OK]");
    }

}
