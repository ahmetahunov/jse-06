package ru.ahmetahunov.tm.command.user;

import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.context.Bootstrap;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.helper.ConsoleHelper;
import ru.ahmetahunov.tm.service.UserService;
import ru.ahmetahunov.tm.util.RoleUtil;

import java.io.IOException;

public class UserChangeAccessAdminCommand extends AbstractCommand {

    public UserChangeAccessAdminCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public boolean isAllowed() {
        User user = bootstrap.getCurrentUser();
        if (user == null) return false;
        if (user.getRole() == Role.ADMINISTRATOR) return true;
        return false;
    }

    @Override
    public String getName() {
        return "change-user-access";
    }

    @Override
    public String getDescription() {
        return "Change selected user's access.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[CHANGE USER'S ACCESS RIGHTS]");
        System.out.print("Please enter user's login: ");
        String login = ConsoleHelper.readMessage().trim();
        UserService userService = bootstrap.getServiceLocator().getUserService();
        User user = userService.findUser(login.toLowerCase());
        if (user == null) {
            System.out.println("Selected user does not exist.");
            return;
        }
        Role role = getRole();
        user.setRole(role);
        bootstrap.getServiceLocator().getUserService().merge(user);
        System.out.println("[OK]");
    }

    private Role getRole() throws IOException {
        System.out.print("Please enter role<User/Administrator>: ");
        String answer = ConsoleHelper.readMessage().trim();
        Role role = RoleUtil.getRole(answer);
        if (role == null) {
            System.out.println("Unknown value.");
            return getRole();
        }
        return role;
    }

}
