package ru.ahmetahunov.tm.command.user;

import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.context.Bootstrap;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.helper.ConsoleHelper;
import ru.ahmetahunov.tm.service.UserService;
import ru.ahmetahunov.tm.util.PassUtil;
import java.io.IOException;

public class UserEditCommand extends AbstractCommand {

    public UserEditCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "user-edit";
    }

    @Override
    public String getDescription() {
        return "Edit user's information.";
    }

    @Override
    public void execute() throws Exception {
        User user = bootstrap.getCurrentUser();
        if (user == null) {
            System.out.println("Please log in.");
            return;
        }
        System.out.println("[USER EDIT]");
        System.out.print("Please enter password: ");
        String password = ConsoleHelper.readMessage();
        password = PassUtil.getHash(password);
        if (!bootstrap.getCurrentUser().getPassword().equals(password)) {
            System.out.println("Wrong password!");
            return;
        }
        System.out.print("Please enter new login: ");
        String login = getLogin();
        user.setLogin(login);
        bootstrap.getServiceLocator().getUserService().merge(user);
        System.out.println("[OK]");
    }

    private String getLogin() throws IOException {
        UserService userService = bootstrap.getServiceLocator().getUserService();
        String login = ConsoleHelper.readMessage().trim();
        User user = userService.findUser(login.toLowerCase());
        while (user != null) {
            System.out.println("User with this login already exists!");
            System.out.print("Please enter login: ");
            login = ConsoleHelper.readMessage().trim();
            user = userService.findUser(login.toLowerCase());
        }
        return login;
    }

}
