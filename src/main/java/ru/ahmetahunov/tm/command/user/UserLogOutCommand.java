package ru.ahmetahunov.tm.command.user;

import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.context.Bootstrap;

public class UserLogOutCommand extends AbstractCommand {

    public UserLogOutCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "log-out";
    }

    @Override
    public String getDescription() {
        return "Log out.";
    }

    @Override
    public void execute() {
        System.out.println("[LOG OUT]");
        bootstrap.setCurrentUser(null);
        System.out.println("Have a nice day!");
    }

}
