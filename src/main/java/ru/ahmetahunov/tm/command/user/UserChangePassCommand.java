package ru.ahmetahunov.tm.command.user;

import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.context.Bootstrap;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.helper.ConsoleHelper;
import ru.ahmetahunov.tm.util.PassUtil;

public class UserChangePassCommand extends AbstractCommand {

    public UserChangePassCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "change-pass";
    }

    @Override
    public String getDescription() {
        return "Change password.";
    }

    @Override
    public void execute() throws Exception {
        User user = bootstrap.getCurrentUser();
        System.out.println("[CHANGE PASSWORD]");
        System.out.print("Please enter old password: ");
        String password = ConsoleHelper.readMessage();
        password = PassUtil.getHash(password);
        if (!user.getPassword().equals(password)) {
            System.out.println("Wrong password!");
            return;
        }
        System.out.print("Please enter new password: ");
        password = ConsoleHelper.readMessage().trim();
        System.out.print("Please enter new password one more time: ");
        String repeatPass = ConsoleHelper.readMessage();
        if (password.isEmpty() || !password.equals(repeatPass)) {
            System.out.println("Passwords do not match!");
            return;
        }
        password = PassUtil.getHash(password);
        user.setPassword(password);
        bootstrap.getServiceLocator().getUserService().merge(user);
        System.out.println("[OK]");
    }

}
