package ru.ahmetahunov.tm.command.user;

import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.context.Bootstrap;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.util.InfoUtil;

public class UserProfileCommand extends AbstractCommand {

    public UserProfileCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "user-profile";
    }

    @Override
    public String getDescription() {
        return "Show user's profile information.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[USER PROFILE]");
        User user = bootstrap.getCurrentUser();
        if (user == null) return;
        System.out.println(InfoUtil.getUserInfo(user));
    }

}
