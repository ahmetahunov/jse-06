package ru.ahmetahunov.tm.command.user;

import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.context.Bootstrap;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.service.UserService;

public class UserListAdminCommand extends AbstractCommand {

    public UserListAdminCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public boolean isAllowed() {
        User user = bootstrap.getCurrentUser();
        if (user == null) return false;
        if (user.getRole() == Role.ADMINISTRATOR) return true;
        return false;
    }

    @Override
    public String getName() {
        return "user-list";
    }

    @Override
    public String getDescription() {
        return "Show all users.";
    }

    @Override
    public void execute() {
        UserService userService = bootstrap.getServiceLocator().getUserService();
        User currentUser = bootstrap.getCurrentUser();
        System.out.println("[USER LIST]");
        int i = 1;
        for (User user : userService.listAllItems()) {
            if (user == currentUser) continue;
            System.out.println(String.format("%d. %s", i++, user.getLogin()));
        }
    }

}
