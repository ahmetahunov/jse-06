package ru.ahmetahunov.tm.command.user;

import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.context.Bootstrap;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.helper.ConsoleHelper;
import ru.ahmetahunov.tm.service.UserService;
import ru.ahmetahunov.tm.util.PassUtil;

public class UserChangePassAdminCommand extends AbstractCommand {

    public UserChangePassAdminCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public boolean isAllowed() {
        User user = bootstrap.getCurrentUser();
        if (user == null) return false;
        if (user.getRole() == Role.ADMINISTRATOR) return true;
        return false;
    }

    @Override
    public String getName() {
        return "change-user-pass";
    }

    @Override
    public String getDescription() {
        return "Change selected user's password.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[CHANGE USER'S PASSWORD]");
        System.out.print("Please enter user's login: ");
        String login = ConsoleHelper.readMessage().trim();
        UserService userService = bootstrap.getServiceLocator().getUserService();
        User user = userService.findUser(login.toLowerCase());
        if (user == null) {
            System.out.println("Selected user does not exist.");
            return;
        }
        System.out.print("Please enter new password: ");
        String password = ConsoleHelper.readMessage().trim();
        System.out.print("Please enter new password one more time: ");
        String repeatPass = ConsoleHelper.readMessage();
        if (password.isEmpty() || !password.equals(repeatPass)) {
            System.out.println("Passwords do not match!");
            return;
        }
        password = PassUtil.getHash(password);
        user.setPassword(password);
        bootstrap.getServiceLocator().getUserService().merge(user);
        System.out.println("[OK]");
    }

}
