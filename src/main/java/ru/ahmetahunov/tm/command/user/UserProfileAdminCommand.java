package ru.ahmetahunov.tm.command.user;

import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.context.Bootstrap;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.helper.ConsoleHelper;
import ru.ahmetahunov.tm.service.UserService;
import ru.ahmetahunov.tm.util.InfoUtil;

public class UserProfileAdminCommand extends AbstractCommand {

    public UserProfileAdminCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public boolean isAllowed() {
        User user = bootstrap.getCurrentUser();
        if (user == null) return false;
        if (user.getRole() == Role.ADMINISTRATOR) return true;
        return false;
    }

    @Override
    public String getName() {
        return "show-user-profile";
    }

    @Override
    public String getDescription() {
        return "Show selected user's profile.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[USER PROFILE]");
        System.out.print("Please enter user login: ");
        String login = ConsoleHelper.readMessage().trim();
        UserService userService = bootstrap.getServiceLocator().getUserService();
        User user = userService.findUser(login.toLowerCase());
        if (user == null) {
            System.out.println("Selected user does not exist.");
            return;
        }
        System.out.println(InfoUtil.getUserInfo(user));
    }

}
