package ru.ahmetahunov.tm.command.project;

import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.context.Bootstrap;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.service.ProjectService;
import ru.ahmetahunov.tm.service.TaskService;

public class ProjectClearCommand extends AbstractCommand {

    public ProjectClearCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "project-clear";
    }

    @Override
    public String getDescription() {
        return "Remove all users's available projects.";
    }

    @Override
    public void execute() {
        ProjectService projectService = bootstrap.getServiceLocator().getProjectService();
        TaskService taskService = bootstrap.getServiceLocator().getTaskService();
        User user = bootstrap.getCurrentUser();
        for (Project project : projectService.listAllItems()) {
            if (project.getUserId().equals(user.getId()))
                taskService.removeAllProjectTasks(project.getId(), user.getId());
        }
        projectService.removeAllUserProjects(user.getId());
        System.out.println("[ALL PROJECTS REMOVED]");
    }

}