package ru.ahmetahunov.tm.command.project;

import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.context.Bootstrap;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.exception.ItemCollisionException;
import ru.ahmetahunov.tm.helper.ConsoleHelper;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.service.ProjectService;
import ru.ahmetahunov.tm.service.TaskService;
import ru.ahmetahunov.tm.util.DateUtil;
import java.io.IOException;

public class ProjectCreateCommand extends AbstractCommand {

    public ProjectCreateCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "project-create";
    }

    @Override
    public String getDescription() {
        return "Create new project.";
    }

    @Override
    public void execute() throws IOException {
        ProjectService projectService = bootstrap.getServiceLocator().getProjectService();
        System.out.println("[PROJECT CREATE]");
        Project project = createNewProject();
        if (project == null) return;
        try {
            projectService.persist(project);
            System.out.println("[OK]");
        } catch (ItemCollisionException e) {
            replaceProject(project);
        }
    }

    private Project createNewProject() throws IOException {
        System.out.print("Please enter project name: ");
        String name = ConsoleHelper.readMessage().trim();
        if (name.isEmpty()) {
            System.out.println("Name cannot be empty.");
            return null;
        }
        Project project = new Project();
        project.setName(name);
        System.out.println("Please enter description:");
        project.setDescription(ConsoleHelper.readMessage());
        System.out.print("Please insert start date(example: 01.01.2020): ");
        project.setStartDate(DateUtil.parseDate(ConsoleHelper.readMessage()));
        System.out.print("Please enter finish date(example: 01.01.2020): ");
        project.setFinishDate(DateUtil.parseDate(ConsoleHelper.readMessage()));
        project.setUserId(bootstrap.getCurrentUser().getId());
        return project;
    }

    private void replaceProject(Project project) throws IOException {
        ProjectService projectService = bootstrap.getServiceLocator().getProjectService();
        TaskService taskService = bootstrap.getServiceLocator().getTaskService();
        User user = bootstrap.getCurrentUser();
        System.out.println("Project with this name already exists.");
        String answer;
        do {
            System.out.println("Do you want replace it?<y/n>");
            answer = ConsoleHelper.readMessage();
            if ("n".equals(answer)) {
                System.out.println("[CANCELLED]");
                return;
            }
        } while (!("y".equals(answer)));
        Project removedProject = projectService.findProject(project.getName(), user.getId());
        if (removedProject != null)
            taskService.removeAllProjectTasks(removedProject.getId(), user.getId());
        projectService.remove(removedProject);
        projectService.merge(project);
        System.out.println("[OK]");
    }

}