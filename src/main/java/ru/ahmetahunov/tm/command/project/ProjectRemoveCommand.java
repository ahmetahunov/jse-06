package ru.ahmetahunov.tm.command.project;

import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.context.Bootstrap;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.helper.ConsoleHelper;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.service.ProjectService;
import ru.ahmetahunov.tm.service.TaskService;
import java.io.IOException;

public class ProjectRemoveCommand extends AbstractCommand {

    public ProjectRemoveCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "project-remove";
    }

    @Override
    public String getDescription() {
        return "Remove selected project.";
    }

    @Override
    public void execute() throws IOException {
        ProjectService projectService = bootstrap.getServiceLocator().getProjectService();
        TaskService taskService = bootstrap.getServiceLocator().getTaskService();
        User user = bootstrap.getCurrentUser();
        System.out.println("[PROJECT REMOVE]");
        System.out.print("Please enter project name: ");
        String projectName = ConsoleHelper.readMessage().trim();
        Project removedProject = projectService.findProject(projectName, user.getId());
        if (removedProject == null) {
            System.out.println("Selected project does not exist.");
            return;
        }
        taskService.removeAllProjectTasks(removedProject.getId(), user.getId());
        projectService.remove(removedProject);
        System.out.println("[OK]");
    }

}