package ru.ahmetahunov.tm.command;

import ru.ahmetahunov.tm.context.Bootstrap;
import ru.ahmetahunov.tm.entity.User;

public abstract class AbstractCommand {

    protected Bootstrap bootstrap;

    public AbstractCommand(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public boolean isAllowed() {
        User user = bootstrap.getCurrentUser();
        if (user == null) return false;
        return true;
    }

    public abstract String getName();

    public abstract String getDescription();

    public abstract void execute() throws Exception;

}