package ru.ahmetahunov.tm.command;

import ru.ahmetahunov.tm.context.Bootstrap;

public class ExitCommand extends AbstractCommand {

    public ExitCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public boolean isAllowed() {
        return true;
    }

    @Override
    public String getName() {
        return "exit";
    }

    @Override
    public String getDescription() {
        return "Exit from Task Manager.";
    }

    public void execute() {
        System.out.println("Have a nice day!");
    }

}