package ru.ahmetahunov.tm.command;

import ru.ahmetahunov.tm.context.Bootstrap;

public class HelpCommand extends AbstractCommand {

    public HelpCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public boolean isAllowed() {
        return true;
    }

    @Override
    public String getName() {
        return "help";
    }

    @Override
    public String getDescription() {
        return "Show all available commands.";
    }

    @Override
    public void execute() {
        for (AbstractCommand command : bootstrap.getCommands().values()) {
            if (!command.isAllowed()) continue;
            System.out.println(String.format("%-19s: %s", command.getName(), command.getDescription()));
        }
    }

}