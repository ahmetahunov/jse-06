package ru.ahmetahunov.tm.command.task;

import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.context.Bootstrap;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.entity.Task;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.helper.ConsoleHelper;
import ru.ahmetahunov.tm.service.ProjectService;
import ru.ahmetahunov.tm.service.TaskService;
import java.io.IOException;

public class TaskMoveCommand extends AbstractCommand {

    public TaskMoveCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "task-move";
    }

    @Override
    public String getDescription() {
        return "Change project for task.";
    }

    @Override
    public void execute() throws IOException {
        TaskService taskService = bootstrap.getServiceLocator().getTaskService();
        User user = bootstrap.getCurrentUser();
        System.out.println("[TASK-MOVE]");
        Project project = getProject("Please enter project name: ", user.getId());
        String projectId = (project == null) ? "" : project.getId();
        System.out.print("Please enter task name: ");
        String taskName = ConsoleHelper.readMessage().trim();
        Task task = taskService.findTask(taskName, projectId, user.getId());
        if (task == null) {
            System.out.println("Selected task does not exist.");
            return;
        }
        project = getProject("Please enter new project name: ", user.getId());
        if (project == null) {
            System.out.println("Selected project does not exist.");
            return;
        }
        task.setProjectId(project.getId());
        taskService.merge(task);
        System.out.println("[OK]");
    }

    private Project getProject(String message, String userId) throws IOException {
        ProjectService projectService = bootstrap.getServiceLocator().getProjectService();
        System.out.print(message);
        String projectName = ConsoleHelper.readMessage().trim();
        return projectService.findProject(projectName, userId);
    }

}
