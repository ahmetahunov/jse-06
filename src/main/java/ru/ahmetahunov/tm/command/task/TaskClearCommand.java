package ru.ahmetahunov.tm.command.task;

import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.context.Bootstrap;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.service.TaskService;

public class TaskClearCommand extends AbstractCommand {

    public TaskClearCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "task-clear";
    }

    @Override
    public String getDescription() {
        return "Remove all user's available tasks.";
    }

    @Override
    public void execute() {
        User user = bootstrap.getCurrentUser();
        TaskService taskService = bootstrap.getServiceLocator().getTaskService();
        taskService.removeAllUserTasks(user.getId());
        System.out.println("[ALL TASKS REMOVED]");
    }

}