package ru.ahmetahunov.tm.command.task;

import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.context.Bootstrap;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.entity.Task;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.helper.ConsoleHelper;
import ru.ahmetahunov.tm.service.ProjectService;
import ru.ahmetahunov.tm.service.TaskService;
import java.io.IOException;

public class TaskRemoveCommand extends AbstractCommand {

    public TaskRemoveCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "task-remove";
    }

    @Override
    public String getDescription() {
        return "Remove selected task.";
    }

    @Override
    public void execute() throws IOException {
        ProjectService projectService = bootstrap.getServiceLocator().getProjectService();
        User user = bootstrap.getCurrentUser();
        TaskService taskService = bootstrap.getServiceLocator().getTaskService();
        System.out.println("[TASK REMOVE]");
        System.out.print("Enter project name or press enter to skip:");
        String projectName = ConsoleHelper.readMessage().trim();
        Project project = projectService.findProject(projectName, user.getId());
        String projectId = (project == null) ? "" : project.getId();
        System.out.print("Enter task name: ");
        String name = ConsoleHelper.readMessage().trim();
        Task task = taskService.findTask(name, projectId, user.getId());
        if (task == null) {
            System.out.println("Selected project does not exist.");
            return;
        }
        taskService.remove(task);
        System.out.println("[OK]");
    }

}