package ru.ahmetahunov.tm.command.task;

import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.context.Bootstrap;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.entity.Task;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.helper.ConsoleHelper;
import ru.ahmetahunov.tm.service.ProjectService;
import ru.ahmetahunov.tm.service.TaskService;
import ru.ahmetahunov.tm.util.InfoUtil;
import java.io.IOException;

public class TaskDescriptionCommand extends AbstractCommand {

    public TaskDescriptionCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "task-description";
    }

    @Override
    public String getDescription() {
        return "Show selected task description.";
    }

    @Override
    public void execute() throws IOException {
        ProjectService projectService = bootstrap.getServiceLocator().getProjectService();
        TaskService taskService = bootstrap.getServiceLocator().getTaskService();
        User user = bootstrap.getCurrentUser();
        System.out.println("[TASK-DESCRIPTION]");
        System.out.print("Please enter project name: ");
        String name = ConsoleHelper.readMessage().trim();
        Project project = projectService.findProject(name, user.getId());
        String projectId = (project == null) ? "" : project.getId();
        System.out.print("Please enter task name: ");
        name = ConsoleHelper.readMessage().trim();
        Task task = taskService.findTask(name, projectId, user.getId());
        if (task == null) {
            System.out.println("Selected task does not exist.");
            return;
        }
        System.out.println(InfoUtil.getItemInfo(task));
    }

}
