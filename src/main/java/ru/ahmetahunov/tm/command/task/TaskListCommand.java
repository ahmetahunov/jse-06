package ru.ahmetahunov.tm.command.task;

import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.context.Bootstrap;
import ru.ahmetahunov.tm.entity.Task;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.service.TaskService;

public class TaskListCommand extends AbstractCommand {

    public TaskListCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "task-list";
    }

    @Override
    public String getDescription() {
        return "Shoe all available tasks.";
    }

    @Override
    public void execute() {
        TaskService taskService = bootstrap.getServiceLocator().getTaskService();
        User user = bootstrap.getCurrentUser();
        int i = 1;
        System.out.println("[TASK LIST]");
        for (Task task : taskService.listAllItems()) {
            if (task.getUserId().equals(user.getId()))
                System.out.println(String.format("%d. %s", i++, task.getName()));
        }
    }

}