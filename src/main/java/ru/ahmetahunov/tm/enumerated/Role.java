package ru.ahmetahunov.tm.enumerated;

public enum Role {

    USER("User"),
    ADMINISTRATOR("Administrator");

    private final String displayName;

    Role(String name) {
        this.displayName = name;
    }

    @Override
    public String toString() {
        return displayName;
    }

}
