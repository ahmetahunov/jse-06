package ru.ahmetahunov.tm.context;

import ru.ahmetahunov.tm.command.*;
import ru.ahmetahunov.tm.command.project.*;
import ru.ahmetahunov.tm.command.task.*;
import ru.ahmetahunov.tm.command.user.*;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.helper.ConsoleHelper;
import ru.ahmetahunov.tm.service.ServiceLocator;
import ru.ahmetahunov.tm.service.UserService;
import ru.ahmetahunov.tm.util.PassUtil;
import java.security.NoSuchAlgorithmException;
import java.util.Map;
import java.util.TreeMap;

public class Bootstrap {

    private Map<String, AbstractCommand> commands;

    private ServiceLocator serviceLocator;

    private User currentUser;

    public ServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    public Map<String, AbstractCommand> getCommands() {
        return commands;
    }

    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    public void init() throws Exception {
        this.commands = new TreeMap<>();
        this.serviceLocator = new ServiceLocator();
        initCommands();
        createDefaultUsers();
        startCycle();
    }

    private void startCycle() throws Exception {
        String operation = "";
        while (!"exit".equals(operation)) {
            System.out.print("Please enter command: ");
            operation = ConsoleHelper.readMessage().trim().toLowerCase();
            execute(operation);
            System.out.println();
        }
        ConsoleHelper.close();
    }

    private void execute(String operation) throws Exception {
        AbstractCommand command = commands.get(operation);
        if (command == null) {
            System.out.println("Unknown command.");
            return;
        }
        if (!command.isAllowed()) {
            System.out.println("You don't have access rights.");
            return;
        }
        command.execute();
    }

    private void initCommands() {
        registry(new HelpCommand(this));
        registry(new ProjectClearCommand(this));
        registry(new ProjectCreateCommand(this));
        registry(new ProjectListCommand(this));
        registry(new ProjectRemoveCommand(this));
        registry(new ProjectSelectCommand(this));
        registry(new ProjectDescriptionCommand(this));
        registry(new TaskClearCommand(this));
        registry(new TaskCreateCommand(this));
        registry(new TaskRemoveCommand(this));
        registry(new TaskListCommand(this));
        registry(new TaskDescriptionCommand(this));
        registry(new TaskMoveCommand(this));
        registry(new ExitCommand(this));
        registry(new UserLogInCommand(this));
        registry(new UserLogOutCommand(this));
        registry(new UserRegistrationCommand(this));
        registry(new UserEditCommand(this));
        registry(new UserProfileCommand(this));
        registry(new UserChangePassCommand(this));
        registry(new UserChangeAccessAdminCommand(this));
        registry(new UserChangePassAdminCommand(this));
        registry(new UserRemoveAdminCommand(this));
        registry(new UserProfileAdminCommand(this));
        registry(new UserListAdminCommand(this));
    }
    
    private void registry(AbstractCommand command) {
        if (command == null) return;
        commands.put(command.getName(), command);
    }

    private void createDefaultUsers() throws NoSuchAlgorithmException {
        UserService userService = serviceLocator.getUserService();
        User user = new User();
        user.setLogin("user");
        user.setPassword(PassUtil.getHash("0000"));
        userService.merge(user);
        user = new User();
        user.setLogin("admin");
        user.setPassword(PassUtil.getHash("admin"));
        user.setRole(Role.ADMINISTRATOR);
        userService.merge(user);
    }

}
