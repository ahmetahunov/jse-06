package ru.ahmetahunov.tm.util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class PassUtil {

    public static String getHash(String password) throws NoSuchAlgorithmException {
        MessageDigest encoder = MessageDigest.getInstance("MD5");
        String salt = "$ert4!";
        String result = password;
        for (int i = 0; i < 10; i++) {
            encoder.update((salt + result).getBytes());
            byte[] buff = encoder.digest();
            result = new BigInteger(1, buff).toString(16);
            encoder.update((result + salt).getBytes());
            buff = encoder.digest();
            result = new BigInteger(1, buff).toString(16);
        }
        return result;
    }

}
