package ru.ahmetahunov.tm.service;

import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.repository.UserRepository;

public class UserService extends AbstractService<User> {

    public UserService(UserRepository repository) {
        super(repository);
    }

    public User findUser(String login) {
        if (login == null || login.isEmpty()) return null;
        for (User user : repository.findAll()) {
            if (user.getLogin().toLowerCase().equals(login))
                return user;
        }
        return null;
    }

}
