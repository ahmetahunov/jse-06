package ru.ahmetahunov.tm.service;

import ru.ahmetahunov.tm.entity.AbstractEntity;
import ru.ahmetahunov.tm.exception.IdCollisionException;
import ru.ahmetahunov.tm.exception.ItemCollisionException;
import ru.ahmetahunov.tm.api.IRepository;
import java.util.Collection;
import java.util.UUID;

public class AbstractService<T extends AbstractEntity> {

    protected final IRepository<T> repository;

    public AbstractService(IRepository<T> repository) {
        this.repository = repository;
    }

    public T persist(T item) throws ItemCollisionException {
        if (item == null) return null;
        if (item.getId() == null || item.getId().isEmpty()) return null;
        if (repository.containsValue(item)) throw new ItemCollisionException();
        try {
            repository.persist(item);
            return item;
        } catch (IdCollisionException e) {
            item.setId(UUID.randomUUID().toString());
            return persist(item);
        }
    }

    public T merge(T item) {
        if (item == null) return null;
        if (item.getId() == null || item.getId().isEmpty()) return null;
        return repository.merge(item);
    }

    public Collection<T> listAllItems() {
        return repository.findAll();
    }

    public void clearRepository() {
        repository.clear();
    }

    public T remove(T item) {
        if (item == null) return null;
        if (item.getId() == null || item.getId().isEmpty()) return null;
        return repository.remove(item.getId());
    }

}
