package ru.ahmetahunov.tm.service;

import ru.ahmetahunov.tm.entity.Task;
import ru.ahmetahunov.tm.repository.TaskRepository;

import java.util.LinkedList;
import java.util.List;

public class TaskService extends AbstractService<Task> {

    private final TaskRepository repository = (TaskRepository) super.repository;

    public TaskService(TaskRepository repository) {
        super(repository);
    }

    public Task findTask(String name, String projectId, String userId) {
        if (name == null || name.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        if (projectId == null) projectId = "";
        return repository.findOne(name, projectId, userId);
    }

    public List<Task> findAll(String projectId, String userId) {
        List<Task> tasks = new LinkedList<>();
        if (projectId == null || projectId.isEmpty()) return tasks;
        if (userId == null || userId.isEmpty()) return tasks;
        return repository.findAll(projectId, userId);
    }

    public void removeAllProjectTasks(String projectId, String userId) {
        if (userId == null || userId.isEmpty()) return;
        if (projectId == null || projectId.isEmpty()) return;
        for (Task task : repository.findAll(userId)) {
            if (task.getProjectId().equals(projectId)) repository.remove(task.getId());
        }
    }

    public void removeAllUserTasks(String userId) {
        if (userId == null || userId.isEmpty()) return;
        listAllItems().removeIf(x -> x.getUserId().equals(userId));
    }

}
