package ru.ahmetahunov.tm.service;

import ru.ahmetahunov.tm.repository.ProjectRepository;
import ru.ahmetahunov.tm.repository.TaskRepository;
import ru.ahmetahunov.tm.repository.UserRepository;

public class ServiceLocator {

    private final ProjectService projectService = new ProjectService(new ProjectRepository());

    private final TaskService taskService = new TaskService(new TaskRepository());

    private final UserService userService = new UserService(new UserRepository());

    public ProjectService getProjectService() {
        return projectService;
    }

    public TaskService getTaskService() {
        return taskService;
    }

    public UserService getUserService() { return userService; }

}
