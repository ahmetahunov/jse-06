package ru.ahmetahunov.tm.service;

import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.repository.ProjectRepository;
import java.util.List;

public class ProjectService extends AbstractService<Project> {

    private final ProjectRepository repository = (ProjectRepository) super.repository;

    public ProjectService(ProjectRepository projectEntityRepository) {
        super(projectEntityRepository);
    }

    public Project findProject(String name, String userId) {
        if (name == null || name.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        return repository.findOne(name, userId);
    }

    public void removeAllUserProjects(String userId) {
        if (userId == null || userId.isEmpty()) return;
        repository.findAll().removeIf(x -> x.getUserId().equals(userId));
    }

    public List<Project> findAll(String userId) {
        if (userId == null || userId.isEmpty()) return null;
        return repository.findAll(userId);
    }

}
